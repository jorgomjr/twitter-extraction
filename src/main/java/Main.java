import java.util.List;
import twitter4j.PagableResponseList;
import twitter4j.Status;
import twitter4j.User;
import java.util.Calendar;
import java.net.URLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import twitter4j.TwitterException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import java.io.FileWriter;
import twitter4j.Twitter;

public class Main
{
    public static Twitter getConnection(final int key) throws IOException {
        String CONSUMER_KEY = "";
        String CONSUMER_SECRET = "";
        String ACCESS_KEY = "";
        String ACCESS_SECRET = "";
        switch (key) {
            case 1: {
                CONSUMER_KEY = "54lqu0b9mMIgvKnSFkOhKZ3PF";
                CONSUMER_SECRET = "XEkbaTHjRBtVoDlxyeyPeqD6Qpj9hN0BSFCoqlEDV1Ak58qWV7";
                ACCESS_KEY = "915613433139224576-FKpaCVmE771l2SYLvYcqxwukpdPbl5f";
                ACCESS_SECRET = "ICJ68YrrPkCnk6lDZzAyq22Hqajua01yrroa32dJ2uY1z";
                break;
            }
            case 2: {
                CONSUMER_KEY = "4gR0TDhvcjG4DsVWdJxNGeCAR";
                CONSUMER_SECRET = "DS4TnG9Mf1KPiicYumKgJSpwRoSrm8sk4lnlBgBir5DBvy8PKD";
                ACCESS_KEY = "915613433139224576-GTa22zXt8vt8GeTrNqO1wAFlKlxy87D";
                ACCESS_SECRET = "ZX1Q81mVMUMFwjxr47Zq4wqtQ8fq1WfvaeLAJ8oyXrPfr";
                break;
            }
            case 3: {
                CONSUMER_KEY = "y5jAR7uZE0SLYVSGAoDYAGFBD";
                CONSUMER_SECRET = "2QK5F1UzsUzyqjvTp5clX9GE1hDgaOPJfLkR8ch2A4boutznJk";
                ACCESS_KEY = "245331596-f8eThCPElnLzLEHsti0D5N0ceCG8u6Zf9I91BAoZ";
                ACCESS_SECRET = "zfebozXGL1gnalOs8Vq8G9vPIFRxeIP6mRkLNkn8RB5st";
                break;
            }
            default: {
                final FileWriter log = new FileWriter("log.log", true);
                log.append("Erro: key not found");
                log.close();
                System.exit(1);
                break;
            }
        }
        final ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(CONSUMER_KEY).setOAuthConsumerSecret(CONSUMER_SECRET).setOAuthAccessToken(ACCESS_KEY).setOAuthAccessTokenSecret(ACCESS_SECRET);
        final TwitterFactory tf = new TwitterFactory(cb.build());
        return tf.getInstance();
    }


    //Antigamente eu usava um jar que precisava de dois argumentos [lista de profiles , id da chave]
    public static void main(final String[] args) throws TwitterException {
        try {
            //Chave que vai ser usada em getConnection()
            final Twitter twitter = getConnection(Integer.parseInt(args[1]));
            try {
                //Lista de IDs
                final FileReader profile = new FileReader(args[0]);
                final BufferedReader bf = new BufferedReader(profile);

                for (String str = "#"; str != null; str = bf.readLine()) {
                    if (!str.startsWith("#")) {
                        getFollowers(twitter, str, -1L, Integer.parseInt(args[1]));
                    }
                }

                bf.close();
                profile.close();
            }
            catch (IllegalStateException ie) {
                if (!twitter.getAuthorization().isEnabled()) {
                    System.out.println("OAuth consumer key/secret is not set.");
                }
                ie.printStackTrace();
            }
        }
        catch (Exception te) {
            System.out.println("TWEET FAILED");
            te.printStackTrace();
        }
    }

    private static boolean netIsAvailable() {
        try {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            return true;
        }
        catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        catch (IOException e2) {
            return false;
        }
    }

    public static void getFollowers(Twitter twitter, final String profile, long cursor, final int key) throws IOException, TwitterException, InterruptedException {
        System.out.println("Listing followers's ids -> " + profile);
        final FileWriter fw = new FileWriter(profile + ".csv");
        final FileWriter log = new FileWriter("log" + key + ".log", true);
        log.write("[" + Calendar.getInstance().getTime().toString() + "] Iniciando profile: " + profile + "\n");
        while (true) {
            PagableResponseList<User> ids;
            try {
                ids = twitter.getFollowersList(profile, cursor);
            }
            catch (TwitterException te) {
                log.write("[" + Calendar.getInstance().getTime().toString() + "] Failed to get followers' ids: " + te.getMessage() + "\n");
                fw.flush();
                log.flush();
                Thread.sleep(900000L);
                while (!netIsAvailable()) {
                    log.write("[" + Calendar.getInstance().getTime().toString() + "] No internet. Waiting 5 minutes to try again..\n");
                    Thread.sleep(300000L);
                }
                twitter = getConnection(key);
                ids = twitter.getFollowersList(profile, cursor);
            }
            log.write("[" + Calendar.getInstance().getTime().toString() + "] Writing " + ids.size() + " data...\n");
            for (final User id : ids) {
                String tweetTime = ";;";
                if (!id.isProtected()) {
                    List<Status> tweetList;
                    try {
                        tweetList = twitter.getUserTimeline(id.getScreenName());
                    }
                    catch (TwitterException ex) {
                        log.write("[" + Calendar.getInstance().getTime().toString() + "] Error: " + ex.getMessage() + " on profile: " + id.getScreenName() + "..Next profile... \n");
                        continue;
                    }
                    if (tweetList.size() > 1) {
                        tweetTime = tweetList.get(0).getCreatedAt() + ";" + tweetList.get(tweetList.size() - 1).getCreatedAt() + ";" + tweetList.size();
                    }
                    else if (tweetList.size() == 1) {
                        tweetTime = tweetList.get(0).getCreatedAt() + ";;1";
                    }
                    else if (tweetList.size() == 0) {
                        tweetTime = ";;0";
                    }
                }
                fw.write(id.getScreenName() + ";" + id.getId() + ";" + id.getFollowersCount() + ";" + id.getFriendsCount() + ";" + id.getLocation() + ";" + id.getLang() + ";" + id.getStatusesCount() + ";" + tweetTime + ";" + id.getCreatedAt() + ";" + id.isVerified() + ";" + id.isDefaultProfile() + ";" + id.isDefaultProfileImage() + "\n");
            }
            if ((cursor = ids.getNextCursor()) == 0L) {
                break;
            }
        }
        fw.close();
        log.close();
    }
}